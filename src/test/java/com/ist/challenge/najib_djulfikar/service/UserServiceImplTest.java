package com.ist.challenge.najib_djulfikar.service;

import com.ist.challenge.najib_djulfikar.Repository.RoleRepository;
import com.ist.challenge.najib_djulfikar.Repository.UserRepository;
import com.ist.challenge.najib_djulfikar.config.CustomAuthentication;
import com.ist.challenge.najib_djulfikar.constant.Erole;
import com.ist.challenge.najib_djulfikar.dto.AuthenticateUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.RegisterUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.UserResponseDTO;
import com.ist.challenge.najib_djulfikar.helper.AuthFailException;
import com.ist.challenge.najib_djulfikar.helper.PasswordSameWithBeforeException;
import com.ist.challenge.najib_djulfikar.helper.ResourceAlreadyExistsException;
import com.ist.challenge.najib_djulfikar.helper.ResourceNotFoundException;
import com.ist.challenge.najib_djulfikar.model.Role;
import com.ist.challenge.najib_djulfikar.model.User;
import com.ist.challenge.najib_djulfikar.service.implementaion.UserServiceImpl;
import com.ist.challenge.najib_djulfikar.util.JwtUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserServiceImpl.class)
public class UserServiceImplTest {
    @MockBean
    UserRepository userRepository;

    @MockBean
    RoleRepository roleRepository;

    @MockBean
    PasswordEncoder encoder;

    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    JwtUtils jwtUtils;

    @Autowired
    UserServiceImpl userService;

    @Test
    void findByUsernameSuccess_Test() throws ResourceNotFoundException {
        User user = User.builder()
                .id(1L)
                .username("example_test")
                .password("password")
                .build();
        Mockito.when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
        User userRes = userService.findByUsername("example_test", "User not found");
        assertEquals(user, userRes);
    }

    @Test
    void findByUsernameNotFoundFail_Test() {
        Throwable e = assertThrows(ResourceNotFoundException.class, () -> {
            Mockito.when(userRepository.findByUsername(anyString()).orElseThrow(() -> new ResourceNotFoundException("User not found"))).thenThrow(ResourceNotFoundException.class);
            userService.findByUsername("example_test", "User not found");
        });
        String expectedMessage = "User not found";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void registerSuccess_Test() throws ResourceAlreadyExistsException, ResourceNotFoundException {
        User user = User.builder()
                .id(1L)
                .username("example_test")
                .password("password")
                .build();
        RegisterUserRequestDTO requestDTO = RegisterUserRequestDTO.builder()
                .username("example_test")
                .password("password")
                .build();
        Role role = Role.builder()
                .id(1L)
                .name(Erole.USER)
                .build();
        Mockito.when(roleRepository.findByName(any())).thenReturn(Optional.of(role));
        Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(Boolean.FALSE);
        Mockito.when(userRepository.save(any())).thenReturn(user);
        User res = userService.register(requestDTO);

        assertEquals(user, res);
    }

    @Test
    void registerUsernameAlreadyExistFail_Test() {
        Throwable e = assertThrows(ResourceAlreadyExistsException.class, () -> {
            User user = User.builder()
                    .id(1L)
                    .username("example_test")
                    .password("password")
                    .build();
            RegisterUserRequestDTO requestDTO = RegisterUserRequestDTO.builder()
                    .username("example_test")
                    .password("password")
                    .build();
            Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(Boolean.TRUE);
            Mockito.when(userRepository.save(any())).thenReturn(user);
            userService.register(requestDTO);
        });

        String expectedMessage = "Username sudah terpakai";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void authenticateSuccess_Test() throws ResourceNotFoundException, AuthFailException {
        UserServiceImpl userServiceSpy = Mockito.spy(userService);
        User user = User.builder()
                .id(1L)
                .username("example_test")
                .password("password")
                .build();
        AuthenticateUserRequestDTO requestDTO = AuthenticateUserRequestDTO.builder()
                .username("example_test")
                .password("password")
                .build();
        Mockito.doReturn(user).when(userServiceSpy).findByUsername(anyString(), anyString());
        Mockito.when(encoder.matches(anyString(), anyString())).thenReturn(Boolean.TRUE);
        Mockito.when(authenticationManager.authenticate(any())).thenReturn(new CustomAuthentication());
        Mockito.when(jwtUtils.generateJwtToken(any())).thenReturn("token");

        String token = userServiceSpy.authenticate(requestDTO);
        assertEquals("token", token);
    }

    @Test
    void authenticationUsernameOrPasswordWrongFail_Test() {
        Throwable e = assertThrows(AuthFailException.class, () -> {
            UserServiceImpl userServiceSpy = Mockito.spy(userService);
            User user = User.builder()
                    .id(1L)
                    .username("example_test")
                    .password("password")
                    .build();
            AuthenticateUserRequestDTO requestDTO = AuthenticateUserRequestDTO.builder()
                    .username("example_test")
                    .password("password1234")
                    .build();
            Mockito.doReturn(user).when(userServiceSpy).findByUsername(anyString(), anyString());
            userServiceSpy.authenticate(requestDTO);
        });

        String expectedMessage = "Username dan/atau password salah!";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void allUserSuccess_Test() throws ResourceNotFoundException {
        User user1 = User.builder()
                .id(1L)
                .username("example_test")
                .password("password")
                .build();
        User user2 = User.builder()
                .id(2L)
                .username("example_test1")
                .password("password")
                .build();
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        Mockito.when(userRepository.findAll()).thenReturn(users);
        List<UserResponseDTO> res = userService.allUser();
        assertNotNull(res);
        assertEquals(2, res.size());
    }

    @Test
    void allUserEmpty_Test() {
        Throwable e = assertThrows(ResourceNotFoundException.class, () -> {
            List<User> users = new ArrayList<>();
            Mockito.when(userRepository.findAll()).thenReturn(users);
            userService.allUser();
        });
        String expectedMessage = "Data tidak ditemukan";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void editUserSuccess_Test() throws ResourceNotFoundException, ResourceAlreadyExistsException, PasswordSameWithBeforeException {
        User user = User.builder()
                .id(1L)
                .username("example_test")
                .password("password")
                .build();
        RegisterUserRequestDTO requestDTO = RegisterUserRequestDTO.builder()
                .username("example_test1")
                .password("password")
                .build();
        Long userId = 1L;
        Mockito.when(userRepository.findById(any())).thenReturn(Optional.of(user));
        userService.editUser(userId, requestDTO);

        assertEquals(userId, user.getId());
        assertEquals(requestDTO.getUsername(), user.getUsername());
    }

    @Test
    void editUserUsernameExistFail_Test() {
        Throwable e = assertThrows(ResourceAlreadyExistsException.class, () -> {
            User user1 = User.builder()
                    .id(1L)
                    .username("example_test")
                    .password("password")
                    .build();
            RegisterUserRequestDTO requestDTO = RegisterUserRequestDTO.builder()
                    .username("example_test1")
                    .password("password")
                    .build();
            Long userId = 1L;
            Mockito.when(userRepository.findById(any())).thenReturn(Optional.of(user1));
            Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(Boolean.TRUE);
            userService.editUser(userId, requestDTO);
        });
        String expectedMessage = "Username sudah terpakai";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void editUserPasswordSameWithBeforeFail_Test() {
        Throwable e = assertThrows(PasswordSameWithBeforeException.class, () -> {
            User user1 = User.builder()
                    .id(1L)
                    .username("example_test")
                    .password("password")
                    .build();
            RegisterUserRequestDTO requestDTO = RegisterUserRequestDTO.builder()
                    .username("example_test")
                    .password("password")
                    .build();
            Long userId = 1L;
            Mockito.when(userRepository.findById(any())).thenReturn(Optional.of(user1));
            Mockito.when(userRepository.existsByUsername(anyString())).thenReturn(Boolean.FALSE);
            Mockito.when(encoder.matches(anyString(), anyString())).thenReturn(Boolean.TRUE);
            userService.editUser(userId, requestDTO);
        });
        String expectedMessage = "Password tidak boleh sama dengan password sebelumnya";
        String actualMessage = e.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

}
