package com.ist.challenge.najib_djulfikar.dto;

import javax.validation.constraints.NotBlank;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AuthenticateUserRequestDTO {
    @NotBlank(message = "kosong")
    private String username;
    
    @NotBlank(message = "kosong")
    private String password;
}
