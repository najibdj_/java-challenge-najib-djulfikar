package com.ist.challenge.najib_djulfikar.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RegisterUserRequestDTO {
    @NotBlank(message = "kosong")
    @Size(max = 25, message = "melebihi batas 25 maksimum karakter")
    private String username;
    
    @NotBlank(message = "kosong")
    @Size(max = 25, message = "melebihi batas 25 maksimum karakter")
    private String password;
}
