package com.ist.challenge.najib_djulfikar.model;

import javax.persistence.*;

import com.ist.challenge.najib_djulfikar.constant.Erole;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter 
@Setter
@Builder
@Entity 
@Table(name = "role", schema = "public")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Erole name;
}
