package com.ist.challenge.najib_djulfikar.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ist.challenge.najib_djulfikar.constant.Erole;
import com.ist.challenge.najib_djulfikar.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(Erole name);
}
