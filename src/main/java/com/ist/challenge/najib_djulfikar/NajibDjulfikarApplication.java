package com.ist.challenge.najib_djulfikar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NajibDjulfikarApplication {

	public static void main(String[] args) {
		SpringApplication.run(NajibDjulfikarApplication.class, args);
	}

}
