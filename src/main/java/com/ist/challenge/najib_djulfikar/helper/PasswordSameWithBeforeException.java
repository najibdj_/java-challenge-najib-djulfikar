package com.ist.challenge.najib_djulfikar.helper;

public class PasswordSameWithBeforeException extends Throwable {

    public PasswordSameWithBeforeException(String message) {
        super(message);
    }
    
}
