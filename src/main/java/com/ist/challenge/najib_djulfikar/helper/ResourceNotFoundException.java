package com.ist.challenge.najib_djulfikar.helper;

public class ResourceNotFoundException extends Throwable{

    public ResourceNotFoundException(String message) {
        super(message);
    }
    
}
