package com.ist.challenge.najib_djulfikar.helper;

public class AuthFailException extends Throwable {

    public AuthFailException(String message) {
        super(message);
    }
    
}
