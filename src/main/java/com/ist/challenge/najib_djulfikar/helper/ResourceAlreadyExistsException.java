package com.ist.challenge.najib_djulfikar.helper;

public class ResourceAlreadyExistsException extends Throwable {

    public ResourceAlreadyExistsException(String message) {
        super(message);
    }
    
}
