package com.ist.challenge.najib_djulfikar.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ist.challenge.najib_djulfikar.dto.AuthenticateUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.RegisterUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.UserResponseDTO;
import com.ist.challenge.najib_djulfikar.helper.AuthFailException;
import com.ist.challenge.najib_djulfikar.helper.PasswordSameWithBeforeException;
import com.ist.challenge.najib_djulfikar.helper.ResourceAlreadyExistsException;
import com.ist.challenge.najib_djulfikar.helper.ResourceNotFoundException;
import com.ist.challenge.najib_djulfikar.helper.ResponseHandler;
import com.ist.challenge.najib_djulfikar.service.implementaion.UserServiceImpl;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    @Autowired
    UserServiceImpl userService;

    /***
     * 
     * @param requestDTO
     * @return
     * @throws ResourceAlreadyExistsException
     * @throws ResourceNotFoundException
     */
    @PostMapping("/auth/register")
    public ResponseEntity<?> signUp(@Valid @RequestBody RegisterUserRequestDTO requestDTO) throws ResourceAlreadyExistsException, ResourceNotFoundException{
        userService.register(requestDTO);
        return ResponseHandler.generateSuccessResponse(HttpStatus.CREATED, "Sukses register", null);
    }

    /***
     * 
     * @param requestDTO
     * @return
     * @throws AuthFailException
     * @throws ResourceNotFoundException
     */
    @PostMapping("/auth/authenticate")
    public ResponseEntity<?> signIn(@Valid @RequestBody AuthenticateUserRequestDTO requestDTO) throws AuthFailException, ResourceNotFoundException{
        String token = userService.authenticate(requestDTO);
        return ResponseHandler.generateSuccessResponse(HttpStatus.OK, "Sukses login", token);
    }

    /***
     * 
     * @return
     * @throws AuthFailException
     * @throws ResourceNotFoundException
     */
    @GetMapping("/all")
    public ResponseEntity<?> allUser() throws AuthFailException, ResourceNotFoundException{
        List<UserResponseDTO> users = userService.allUser();
        return ResponseHandler.generateSuccessResponse(HttpStatus.OK, "Berhasil mendapatkan data", users);
    }

    @PutMapping("/edit/{userId}")
    public ResponseEntity<?> editUser(@PathVariable("userId") Long userId, @Valid @RequestBody RegisterUserRequestDTO requestDTO) throws ResourceNotFoundException, ResourceAlreadyExistsException, PasswordSameWithBeforeException{
        userService.editUser(userId, requestDTO);
        return ResponseHandler.generateSuccessResponse(HttpStatus.CREATED, "Sukses edit user", null);
    }

}
