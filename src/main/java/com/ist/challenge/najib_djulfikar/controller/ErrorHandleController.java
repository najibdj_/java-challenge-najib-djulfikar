package com.ist.challenge.najib_djulfikar.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ist.challenge.najib_djulfikar.helper.AuthFailException;
import com.ist.challenge.najib_djulfikar.helper.PasswordSameWithBeforeException;
import com.ist.challenge.najib_djulfikar.helper.ResourceAlreadyExistsException;
import com.ist.challenge.najib_djulfikar.helper.ResourceNotFoundException;
import com.ist.challenge.najib_djulfikar.helper.ResponseHandler;

@ControllerAdvice
public class ErrorHandleController extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseHandler.generateErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage(), 1000, "Maformed JSON");
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException e) {
        return ResponseHandler.generateErrorResponse(HttpStatus.NOT_FOUND, e.getMessage(), 1001, "Resource not found");
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<?> handleResourceAlreadyExistException(ResourceAlreadyExistsException e) {
        return ResponseHandler.generateErrorResponse(HttpStatus.CONFLICT, e.getMessage(), 1002, "Resource already exists");
    }

    @ExceptionHandler(AuthFailException.class)
    public ResponseEntity<?> handleAuthFailException(AuthFailException e) {
        return ResponseHandler.generateErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage(), 1003,   "Resource already exists");
    }

    @ExceptionHandler(PasswordSameWithBeforeException.class)
    public ResponseEntity<?> handlePasswordSameWithBeforeException(PasswordSameWithBeforeException e) {
        return ResponseHandler.generateErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage(), 1003,   "Resource already exists");
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> messages = new ArrayList<>();
        ex.getFieldErrors().forEach(err -> {
            messages.add(err.getField() + " " + err.getDefaultMessage());
        });
        return ResponseHandler.generateValidationErrorResponse(HttpStatus.BAD_REQUEST, messages, 1003, "Validation error");
    }
}
