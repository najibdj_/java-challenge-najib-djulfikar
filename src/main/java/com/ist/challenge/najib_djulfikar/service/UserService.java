package com.ist.challenge.najib_djulfikar.service;

import java.util.List;

import com.ist.challenge.najib_djulfikar.dto.AuthenticateUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.RegisterUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.UserResponseDTO;
import com.ist.challenge.najib_djulfikar.helper.AuthFailException;
import com.ist.challenge.najib_djulfikar.helper.PasswordSameWithBeforeException;
import com.ist.challenge.najib_djulfikar.helper.ResourceAlreadyExistsException;
import com.ist.challenge.najib_djulfikar.helper.ResourceNotFoundException;
import com.ist.challenge.najib_djulfikar.model.User;

public interface UserService {

    User findByUsername(String username, String errorMessage) throws ResourceNotFoundException;

    User register(RegisterUserRequestDTO requestDTO) throws ResourceAlreadyExistsException, ResourceNotFoundException;
    
    String authenticate(AuthenticateUserRequestDTO requestDTO) throws AuthFailException, ResourceNotFoundException;

    List<UserResponseDTO> allUser() throws ResourceNotFoundException;

    void editUser(Long userId, RegisterUserRequestDTO requestDTO) throws ResourceNotFoundException, ResourceAlreadyExistsException, PasswordSameWithBeforeException;

}
