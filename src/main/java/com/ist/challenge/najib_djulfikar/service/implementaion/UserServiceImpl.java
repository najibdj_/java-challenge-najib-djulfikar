package com.ist.challenge.najib_djulfikar.service.implementaion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ist.challenge.najib_djulfikar.Repository.RoleRepository;
import com.ist.challenge.najib_djulfikar.Repository.UserRepository;
import com.ist.challenge.najib_djulfikar.constant.Erole;
import com.ist.challenge.najib_djulfikar.dto.AuthenticateUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.RegisterUserRequestDTO;
import com.ist.challenge.najib_djulfikar.dto.UserResponseDTO;
import com.ist.challenge.najib_djulfikar.helper.AuthFailException;
import com.ist.challenge.najib_djulfikar.helper.PasswordSameWithBeforeException;
import com.ist.challenge.najib_djulfikar.helper.ResourceAlreadyExistsException;
import com.ist.challenge.najib_djulfikar.helper.ResourceNotFoundException;
import com.ist.challenge.najib_djulfikar.model.Role;
import com.ist.challenge.najib_djulfikar.model.User;
import com.ist.challenge.najib_djulfikar.service.UserService;
import com.ist.challenge.najib_djulfikar.util.JwtUtils;

@Service
public class UserServiceImpl implements UserService {
    
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @Override
    public User findByUsername(String username, String errorMessage) throws ResourceNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException(errorMessage));
    }
    
    @Override
    public User register(RegisterUserRequestDTO requestDTO) throws ResourceAlreadyExistsException, ResourceNotFoundException {
        boolean checkUsername = userRepository.existsByUsername(requestDTO.getUsername());
        if (Boolean.TRUE.equals(checkUsername)) {
            throw new ResourceAlreadyExistsException("Username sudah terpakai");
        }
        User user = User.builder()
            .username(requestDTO.getUsername())
            .password(encoder.encode(requestDTO.getPassword()))
            .build();

        Set<Role> roles = new HashSet<>();
        Role role = roleRepository.findByName(Erole.USER).orElseThrow(() -> new ResourceNotFoundException("Role tidak ditemukan"));
        roles.add(role);
        user.setRoles(roles);

        return userRepository.save(user);
    }

    @Override
    public String authenticate(AuthenticateUserRequestDTO requestDTO) throws AuthFailException, ResourceNotFoundException {
        User user = findByUsername(requestDTO.getUsername(), "Username dan/atau password salah!");
        Boolean isPasswordCorrect = encoder.matches(requestDTO.getPassword(), user.getPassword());
        if (Boolean.FALSE.equals(isPasswordCorrect)) {
            throw new AuthFailException("Username dan/atau password salah!");
        }
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestDTO.getUsername(), requestDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtUtils.generateJwtToken(authentication);
    }

    @Override
    public List<UserResponseDTO> allUser() throws ResourceNotFoundException {
        List<User> users = userRepository.findAll();

        if (users.isEmpty()) {
            throw new ResourceNotFoundException("Data tidak ditemukan");
        }

        List<UserResponseDTO> responseDTOs = new ArrayList<>();
        users.forEach(user -> {
            UserResponseDTO responseDTO = UserResponseDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .build();
            responseDTOs.add(responseDTO);
        });
        return responseDTOs;
    }

    @Override
    public void editUser(Long userId, RegisterUserRequestDTO requestDTO) throws ResourceNotFoundException, ResourceAlreadyExistsException, PasswordSameWithBeforeException {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        if (!requestDTO.getUsername().equals(user.getUsername())) {
            if (Boolean.TRUE.equals(userRepository.existsByUsername(requestDTO.getUsername()))) {
                throw new ResourceAlreadyExistsException("Username sudah terpakai");
            }
        }
        Boolean isPasswordCorrect = encoder.matches(requestDTO.getPassword(), user.getPassword());
        if (Boolean.TRUE.equals(isPasswordCorrect)) {
            throw new PasswordSameWithBeforeException("Password tidak boleh sama dengan password sebelumnya");
        }
        user.setUsername(requestDTO.getUsername());
        user.setPassword(encoder.encode(requestDTO.getPassword()));
        userRepository.save(user);
    }

}   
